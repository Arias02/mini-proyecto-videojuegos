using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private InputManager inputManager;
    private PlayerMovement playerMovement;
    private ThirdPersonShooter thirdPersonShooter;
    private void Awake()
    {
        inputManager = GetComponent<InputManager>();
        playerMovement = GetComponent<PlayerMovement>();
        thirdPersonShooter = GetComponent<ThirdPersonShooter>();

    }

    private void Update()
    {
        inputManager.HandleAllInputs();
        thirdPersonShooter.HandleAllShooting();
    }

    private void FixedUpdate()
    {
        playerMovement.HandleAllMovement();

    }
    
}
