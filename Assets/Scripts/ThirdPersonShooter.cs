using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.EventSystems;

public class ThirdPersonShooter : MonoBehaviour
{
    private PlayerMovement playerMovement;
    [SerializeField] private CinemachineFreeLook mainCamera;
    [SerializeField] private LayerMask aimColliderLayerMask = new LayerMask();
    [SerializeField] private Transform debugTransform;
    [SerializeField] private Transform hitTransform = null;
    [SerializeField] private Transform vfxHitRed;
    [SerializeField] private Transform vfxHitGreen;
    [SerializeField] private GameObject Weapon1;
    [SerializeField] private GameObject Weapon2;
    [SerializeField] private GameObject playerRender;
    [SerializeField] private AudioClip SingleShot;
    [SerializeField] private AudioClip Hurt;
    [SerializeField] private AudioClip Dead;
    [SerializeField] private AudioClip Healing;
    [SerializeField] private AudioClip Ammunition;

    public AudioSource SFX;

    public float level = 1;

    private bool Machinegun = false;
    
    private float FOV = 40f;
    private float aimFOV = 15f;
    public float fovSmoothSpeed = 7f;
    private Vector3 aimDirection;
    public float turnSmoothTime = 0.1f;
    private float turnSmoothVelocity;
    public float Ammo = 90;
    public float hp = 100;
    private bool inmortal = false;
    
    
    
    private InputManager inputManager;
    
    private void Update()
    {
        Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
        
        if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, aimColliderLayerMask))
        {
            debugTransform.position = raycastHit.point;
            hitTransform = raycastHit.transform;
            
        }

        if (inputManager.shoot == false)
        {
            if (Machinegun == false)
            {
                playerRender.GetComponent<Animator>().SetBool("Firing", false);
            } 

        }
    }

    

    private void Awake()
    {
        inputManager = GetComponent<InputManager>(); 
        playerMovement = GetComponent<PlayerMovement>();
        SFX = GetComponent<AudioSource>();
    }
   

    public void HandleAllShooting()
    {
        Aim();
        Shoot();
        ChangeWeapon();
    }

    public void ChangeWeapon()
    {
        if (inputManager.mouseScrollValue == 1)
        {
            Machinegun = true;
            Weapon1.SetActive(true);
            Weapon2.SetActive(false);
        }
        else
        {
            Machinegun = false;
            Weapon2.SetActive(true);
            Weapon1.SetActive(false);
        }
    }

    public void Shoot()
    {
        if (inputManager.shoot )
        {
            if (inputManager.aim)
            {
                
                if (Ammo > 0)
                {
                    if (Machinegun == false)
                    {
                        playerRender.GetComponent<Animator>().SetBool("Firing", true);
                    } 


                    if (hitTransform != null)
                    {
                        SFX.PlayOneShot(SingleShot);
                       
                        if (hitTransform.CompareTag("Enemy"))
                        {
                            MaquinaDeEstados enemy = hitTransform.GetComponent<MaquinaDeEstados>();
                            Debug.LogError("enemy",enemy);
                            enemy.Restarvida(); 
                            Instantiate(vfxHitGreen, debugTransform.position, Quaternion.identity);
   
                        }
                        else
                        {
                      
                            Instantiate(vfxHitRed,debugTransform.position , Quaternion.identity);
                            Debug.LogError("error",hitTransform);
                        }
                    } 
                    
                }
                Wasteammo();
                
            }
            inputManager.shoot = false;
            
        }
        
    }
    

    public void Aim()
    {

        if (inputManager.aim)
        {
            //Debug.Log("aiming");
            mainCamera.m_XAxis.m_MaxSpeed = 40f;
            mainCamera.m_YAxis.m_MaxSpeed = 0.5f;
            mainCamera.m_Lens.FieldOfView = Mathf.Lerp(mainCamera.m_Lens.FieldOfView, aimFOV, fovSmoothSpeed * Time.deltaTime);
        }
        else
        {
            //Debug.Log("Not aiming");
            mainCamera.m_YAxis.m_MaxSpeed = 1.3f;
            mainCamera.m_XAxis.m_MaxSpeed = 150f;
            mainCamera.m_Lens.FieldOfView = Mathf.Lerp(mainCamera.m_Lens.FieldOfView, FOV, fovSmoothSpeed * Time.deltaTime);

        }
       
    }

    public void Addammo()
    {
        if (Ammo < 80)
        { 
            Ammo += 10;
        }else if (Ammo >= 80)
        {
            Ammo = 90;
        }
        SFX.PlayOneShot(Ammunition);
    }
    
    public void Wasteammo()
    {
        if (Ammo > 0)
        {
            Ammo--;
        }
    }

    public void Sumarvida()
    {
        if (hp < 80)
        {
            hp += 20;
        }else if (hp >= 80)
        {
            hp = 100;
        }
        SFX.PlayOneShot(Healing);
    }
    public void Restarvida()
    {
        if (inmortal == false)
        {
            if (hp > 20)
            {
                inmortal = true;
                hp -= 20;
                StartCoroutine(Damagecooldown());
                SFX.PlayOneShot(Hurt);
                
            }
            else if (hp <= 20)
            {
                inmortal = true;
                hp = 100;
                playerMovement.Respawn();
                StartCoroutine(Damagecooldown());
                SFX.PlayOneShot(Dead);
                
                
            
            }
        }
        
        
        
        
    } 
    public void Matar()
    {

        hp = 100; 
        playerMovement.Respawn();
        
    }

    IEnumerator Damagecooldown()
    {
        
        yield return new WaitForSeconds(0.7f);
        inmortal = false;
    }
    
}
