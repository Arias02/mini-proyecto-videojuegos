using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoyOverTime : MonoBehaviour
{
    private void Awake()
    {
        StartCoroutine(DestroyOverTime());
    }

    IEnumerator DestroyOverTime()
    {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }
}
