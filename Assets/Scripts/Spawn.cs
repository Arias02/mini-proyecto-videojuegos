using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public PlayerMovement Move;
    private void Awake()
    {
        GameObject Player = GameObject.Find("Player");
        Move = Player.GetComponent<PlayerMovement>();
        Move.respawn = transform;
    }

    
}
