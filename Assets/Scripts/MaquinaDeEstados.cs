using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Cinemachine;
using UnityEngine;
using Pathfinding;
using Random = System.Random;

public class MaquinaDeEstados : MonoBehaviour
{
    [SerializeField] private Transform waypointA; //Transform del primer waypoint 
    [SerializeField] private Transform waypointB; //Transform del segundo waypoint 
    [SerializeField] private Transform explosion;  
    [SerializeField] private GameObject ear; //Objeto hijo con un colider de esfera que hace de area de audición
    [SerializeField] private GameObject healthBox; 
    [SerializeField] private GameObject ammoBox;
    [SerializeField] private GameObject enemyRenderer;
    private Random random = new Random();
    private int valor;

    //Valores que determinan el rango en el que casteará rayos para comprobar que el player está en el rango de visión
    
    float ViewRange = 55;
    float hHalfFov = 55; 
    float vHalfFov = 55;
    public float vida = 5;
    
    
    public AIDestinationSetter aiDestinationSetter; //referencia al script que controla el destino al que tiene que llegar el
                                                    //enemigo para poder acceder al componente de dicho script que permite cambiar el objetivo de la ia 

    public void Awake()
    {
        //se accede al script al iniciar el programa
        aiDestinationSetter = GetComponent<AIDestinationSetter>();
    }

    public void Update()
    {
        CanSeePlayer();
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ThirdPersonShooter player = collision.gameObject.GetComponent<ThirdPersonShooter>();
            player.Restarvida();
            
            
        }
       

       
    }

    private void CanSeePlayer() //función encargada de realizar la comprovación del fov, tras buscar en internet como realizar un raycast en 180 grados
    {
        enemyRenderer.GetComponent<Animator>().SetBool("Player", true);

        // castea una esfera para encontrar todos los coliders dentro del rango de visión en 360 grados
            var hits = Physics.OverlapSphere(transform.position, ViewRange);

            foreach (var hit in hits) {
                if ( hit.gameObject == this.gameObject ) continue; //elimina todas las colisiones que pueda realizar consigo mismo

                // Comprobación y filtrado del campo de visión
                var direction = (transform.position - hit.transform.position);
                var hDirn = Vector3.ProjectOnPlane(direction, transform.up).normalized; // proyecta rayos relativos a XY del transform del objeto para comporbar verticalmente
                var vDirn = Vector3.ProjectOnPlane(direction, transform.right).normalized; // proyecta rayos relativos a los ejes YZ del transform del objeto para comprobar horizontalmente

                var hOffset = Vector3.Dot(hDirn, transform.forward) * Mathf.Rad2Deg; // calcula el desfase angular horizontal en grados
                var vOffset = Vector3.Dot(vDirn, transform.forward) * Mathf.Rad2Deg; // calcula el desfase angular vertical en grados

                if (hOffset > hHalfFov || vOffset > vHalfFov) continue; // con los valores obtenidos filtra cuales rayos se guardan y cuales están fuera del FOV

                Debug.DrawLine(transform.position, hit.transform.position, Color.red); // dibuja los rayos en la escena para poder ver que funciona correctamente

                if (hit.gameObject.CompareTag("Player")) // establece como target de la IA el transform del jugador si este está en el campo de visión de esta
                {
                    aiDestinationSetter.target = hit.gameObject.transform;
                    
                    //Debug.Log("voy a player"); // escribe un mensaje en consola para poder comprobar que está fucnionando correctamente
                }
                
                
            }
        
    }

    public void Cambiar() // metodo que se encarga de cambiar entre 2 checkpoints
    {
        if (aiDestinationSetter.target == waypointA) // en caso de estar en el waypoint A, se cambia al wayponint B
        {
            aiDestinationSetter.target = waypointB;
            //Debug.Log("voy a B"); // mensaje de que se dirige al waypoint B
        } 
        else if (aiDestinationSetter.target == waypointB) // en caso de estar en el waypoint B, cambia al waypoint A
        {
            aiDestinationSetter.target = waypointA;
            //Debug.Log("voy a A"); // mensaje de que se dirige al waypoint A
        }
        
    }

    public void Restarvida()
    {
        if (vida > 1)
        {
            vida--;
        }else if (vida <= 1)
        {
            Destroyenemy();
        }
    }

    public void Destroyenemy()
    {
        
        Vector3 drop = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
        valor = random.Next(0,2);
        if (valor > 0)
        {
            Instantiate(healthBox, drop, Quaternion.identity);
            Debug.Log(valor);
        }
        if (valor < 1)
        {
            Instantiate(ammoBox, drop, Quaternion.identity);
            Debug.Log(valor);
        }
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }


    IEnumerator HitCooldown(Collision collision)
    {
        yield return new WaitForSeconds(1.5f);
        
        
    }


}