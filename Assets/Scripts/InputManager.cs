using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class InputManager : MonoBehaviour
{ 
    PlayerControls playerControls;

    public Vector2 movementInput;
    public Vector2 Scroll;
    public bool jump;
    public bool shoot;
    public bool aim;
    public float verticalInput;
    public float horizontalInput;
    public bool walk;
    public bool run;
    public bool pause;
    public float mouseScrollValue;
    public bool Scrollperformed;
    public bool bulletShot = false;
    [SerializeField] private GameObject playerRender;
    [SerializeField] private AudioClip Auto;

    private AudioSource gun;
    private void OnEnable()
    {
        if (playerControls == null)
        {
            playerControls = new PlayerControls();

            playerControls.PlayerMovement.Movement.performed += i => movementInput = i.ReadValue<Vector2>();
            playerControls.PlayerMovement.Scroll.performed += i => Scroll = i.ReadValue<Vector2>();
           
        }
        
        playerControls.Enable();
    }

    private void OnDisable()
    {
         playerControls.Disable();
    }

    public void HandleAllInputs()
    {
        HandleMovementInput();
    }

    private void HandleMovementInput()
    {

        mouseScrollValue = Scroll.y;
        
        
        verticalInput = movementInput.y;
        horizontalInput = movementInput.x;
        if (mouseScrollValue == 1)
        {
            if (playerControls.PlayerMovement.Shoot.IsPressed())
            {
                StartCoroutine(ShootCooldown());
            }
            else
            {
                playerRender.GetComponent<Animator>().SetBool("Firing", false);
                StopCoroutine(ShootCooldown());
                bulletShot = false;
                
                
            }
        }
        else
        {
            if (playerControls.PlayerMovement.Shoot.triggered)
            {
                shoot = true;
            }
        }
        

        if (playerControls.PlayerMovement.Aim.IsPressed())
        {
            aim = true;
        }
        else
        {
            aim = false;
        }

        if (playerControls.PlayerMovement.Jump.IsPressed())
        {
            jump = true;
        }
        else
        {
            jump = false;
        }

        if (playerControls.PlayerMovement.Run.IsPressed())
        {
            run = true;
        }
        else
        {
            run = false;
        }
        
        if (playerControls.PlayerMovement.Pause.triggered)
        {
            pause = !pause;
            Cursor.visible = !Cursor.visible;
        }

        
    }

    IEnumerator ShootCooldown()
    {
        if (bulletShot == false)
        {
            playerRender.GetComponent<Animator>().SetBool("Firing", true);
            bulletShot = true;
            shoot = true;
            yield return new WaitForSeconds(0.1f);
            bulletShot = false;
        }
        
    }
    
    
}
