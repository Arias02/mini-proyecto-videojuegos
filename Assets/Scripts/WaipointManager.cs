using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaipointManager : MonoBehaviour
{
    private MaquinaDeEstados enemy; // objeto de la clase MaqinaDeEstados
    private void OnTriggerEnter(Collider other) // cuando algo entre al trigger ejecutará el codigo
    {
        if (other.CompareTag("Enemy")) // comprueba si lo que ha entrado en el trigger tiene el tag enemigo, si no, no hará nada
        {
            Debug.Log("estoy en el objetivo"); // mensaje en consola para representar que se encuentra en el checkpoint
            enemy = other.GetComponent<MaquinaDeEstados>(); // se adquiere el componente maquina de estados del objeto que ha entrado en el trigger y se le asigna valor al objeto declarada al inicio
            enemy.Cambiar(); // se llama al metodo cambiar del objeto enemy 
            
        }
    }
}
