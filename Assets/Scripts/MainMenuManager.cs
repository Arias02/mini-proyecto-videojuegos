using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
   

    private void Awake()
    {
        
    }

    public void LoadGame()
    {
        Debug.Log("Loading game");
        SceneLoaderSimple.Instance.LoadGameScene1();
        
        SceneLoaderSimple.Instance.RemoveMainMenu();
       
    }

    public void ExitGame()
    {
        Debug.Log("QUITTING");
        Application.Quit();
    }
    
    
}
