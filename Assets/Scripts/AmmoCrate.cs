using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoCrate : MonoBehaviour
{
    private ThirdPersonShooter player;
    [SerializeField] private Transform vfxAmmo;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            player = other.GetComponent<ThirdPersonShooter>();
            player.Addammo();
            Instantiate(vfxAmmo,transform.position , Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
