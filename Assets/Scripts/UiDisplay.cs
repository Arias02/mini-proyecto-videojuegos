using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UiDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI ammo;
    [SerializeField] private TextMeshProUGUI hp;
    private ThirdPersonShooter playerShooter;

    private void Awake()
    {
        playerShooter = GameObject.Find("Player").GetComponent<ThirdPersonShooter>();
    }

    private void Update()
    {
        ammo.text = playerShooter.Ammo.ToString();
        String health = playerShooter.hp.ToString() + "% HP";
        hp.text = health;

    }
}
