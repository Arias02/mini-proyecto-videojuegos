using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour
{
    public bool Pulsado = false;

    private AudioSource Interaction;

    [SerializeField] private AudioClip Button;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Input.GetKey("e"))
            {
                Pulsado = true;
                other.GetComponent<ThirdPersonShooter>().SFX.PlayOneShot(Button);
            }
            
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
