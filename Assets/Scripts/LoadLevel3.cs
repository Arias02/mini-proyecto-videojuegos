using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevel3 : MonoBehaviour
{

    private bool loaded = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && loaded == false)
        {
            ThirdPersonShooter player = other.GetComponent<ThirdPersonShooter>();
            player.level = 3;
            SceneLoaderSimple.Instance.LoadGameScene3();
            loaded = true;
        }
    }
    
}
