using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Input = UnityEngine.Windows.Input;

public class PauseMenu1 : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenuBackground;
    [SerializeField] private GameObject pauseMenuRestart;
    [SerializeField] private GameObject pauseMenuQuit;
    [SerializeField] private GameObject pauseMenuClose;
    private GameObject UI;
    
    

    private InputManager inputManager;
    private ThirdPersonShooter TPShooter;
    private PlayerMovement Move;
    private FinalScreenTrigger End;

    private void Awake()
    {
        UI = GameObject.Find("UI");
        UI.SetActive(true);
        inputManager = GameObject.Find("Player").GetComponent<InputManager>();
        TPShooter = GameObject.Find("Player").GetComponent<ThirdPersonShooter>();
        Move = GameObject.Find("Player").GetComponent<PlayerMovement>();
        End = GameObject.Find("Final Screen Trigger").GetComponent<FinalScreenTrigger>();
    }

    private void Update()
    {
        if (inputManager.pause && End.finalScreen == false)
        {
            pauseMenuBackground.SetActive(true);
            pauseMenuRestart.SetActive(true);
            pauseMenuQuit.SetActive(true);
            pauseMenuClose.SetActive(true);
            UI.SetActive(false);
            Time.timeScale = 0;

        }
        else if (End.finalScreen == false)
        {
            pauseMenuBackground.SetActive(false);
            pauseMenuRestart.SetActive(false);
            pauseMenuQuit.SetActive(false);
            pauseMenuClose.SetActive(false);
            UI.SetActive(true);
            Time.timeScale = 1;

        }
    }

    public void RestartCurrentLevel()
    {
        if (TPShooter.level == 1)
        {
            Restart1();
        }
        if (TPShooter.level == 2)
        {
            Restart2();
        }
        if (TPShooter.level == 3)
        {
            Restart3();
        }
    }
    public void Restart1()
    {
        Debug.Log("Loading game");
        SceneLoaderSimple.Instance.RemoveGameScene1();
        SceneLoaderSimple.Instance.LoadGameScene1();
        Move.Respawn();
        



    }
    public void Restart2()
    {
        Debug.Log("Loading game");
        
        SceneLoaderSimple.Instance.RemoveGameScene2();
        SceneLoaderSimple.Instance.LoadGameScene2();
        Move.Respawn();
        
       
    }
    public void Restart3()
    {
        Debug.Log("Loading game");
        
        SceneLoaderSimple.Instance.RemoveGameScene3();
        SceneLoaderSimple.Instance.LoadGameScene3();
        Move.Respawn();
        
       
    }
    

    public void ExitMainMenu()
    {
        SceneLoaderSimple.Instance.LoadMainMenu();
        if (TPShooter.level == 1)
        {
            SceneLoaderSimple.Instance.RemoveGameScene1();
        }
        if (TPShooter.level == 2)
        {
            SceneLoaderSimple.Instance.RemoveGameScene2();
        }
        if (TPShooter.level == 3)
        {
            SceneLoaderSimple.Instance.RemoveGameScene3();
        }
        Debug.Log("QUITTING");
        inputManager.pause = false;
        TPShooter.level = 1;

    }

    public void CloseMenu()
    {
        inputManager.pause = false;
    }
    
    
}
