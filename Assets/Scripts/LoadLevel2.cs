using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevel2 : MonoBehaviour
{
    private bool loaded = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && loaded == false)
        {
            ThirdPersonShooter player = other.GetComponent<ThirdPersonShooter>();
            player.level = 2;
            SceneLoaderSimple.Instance.LoadGameScene2();
            loaded = true;
        }
    } 
}
