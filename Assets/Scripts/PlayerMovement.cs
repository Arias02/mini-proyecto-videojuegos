using System;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
   InputManager inputManager;
   
    
  public Vector3 moveDirection;
  Vector3 move;
  [SerializeField] Transform cameraObject;
  CharacterController characterController;

  public float movementSpeed = 7f;
  public float turnSmoothTime = 0.1f;
  private float turnSmoothVelocity;
  private float gravity;
  private float gravitySpeed = -9.81f;
  
  [SerializeField] float gravityMultiplier = 1f;
  [SerializeField] float jumpHeight= 1f;
  public Transform respawn;
  public Vector3 position;
  [SerializeField] private GameObject playerRender;
  [SerializeField] private AudioSource walkSFX;
  [SerializeField] private AudioClip Walk;
  private bool audioPlaying;
  
  private void Awake()
  {
      inputManager = GetComponent<InputManager>();
      characterController = GetComponent<CharacterController>();
      respawn = GameObject.Find("Respawn").GetComponent<Transform>();
      
      



  }

  public void HandleAllMovement()
  {
      HandleRotation();
      HandleGravity();
      HandleMovement();
     
  }

  public void HandleGravity()
  {
      if (inputManager.jump && characterController.isGrounded)
      {
          gravity += jumpHeight;
      }
      else if (characterController.isGrounded)
      {
          gravity = -1f;
      }
      else
      {
          gravity += gravitySpeed * gravityMultiplier * Time.deltaTime;
      }
  }
  public void HandleRotation()
  {
      
      float targetAngle = Mathf.Atan2(inputManager.movementInput.x, inputManager.movementInput.y) * Mathf.Rad2Deg + cameraObject.eulerAngles.y;
      float targetAngleRotate = Mathf.Atan2(0, 0) * Mathf.Rad2Deg + cameraObject.eulerAngles.y;
      float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngleRotate,ref turnSmoothVelocity, turnSmoothTime);
      transform.rotation = Quaternion.Euler(0f, angle,0f);
      moveDirection = Quaternion.Euler(0f, targetAngle, 0) * Vector3.forward;
      
      Vector3 input = new Vector3(inputManager.movementInput.x, 0f, inputManager.movementInput.y);
      if(input.magnitude!= 0f)
      {
          move = new Vector3(moveDirection.x, gravity, moveDirection.z);
          playerRender.GetComponent<Animator>().SetBool("Running", true);

          if (audioPlaying == false)
          {
              walkSFX.Play();
              audioPlaying = true;
          }
              
          
          

      }
      else
      {
          
          walkSFX.Stop();
          audioPlaying = false;
          move = new Vector3(0f, gravity, 0f);
          playerRender.GetComponent<Animator>().SetBool("Running", false);
         
          
      }
  }

  


  public void HandleMovement()
  {
      if (inputManager.run)
      {
          movementSpeed = 15f;
      }
      else
      {
          movementSpeed = 9f;
      }
      
      characterController.Move(move * (Time.deltaTime * movementSpeed));

  }

  public void Respawn()
  {
      
      Debug.Log("respawn");

      position = respawn.transform.position;
      transform.position = new Vector3(position.x, position.y, position.z);
  }
}
