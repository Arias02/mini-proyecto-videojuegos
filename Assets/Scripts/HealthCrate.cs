using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCrate : MonoBehaviour
{
    private ThirdPersonShooter player;
    [SerializeField] private Transform vfxHealth;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            player = other.GetComponent<ThirdPersonShooter>();
            player.Sumarvida();
            Instantiate(vfxHealth,transform.position , Quaternion.identity);
            Destroy(gameObject);
        }
        
    }
    
}
