using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{

    [SerializeField] private GameObject button1;
    [SerializeField] private GameObject button2;

    private ButtonScript DoorButton1;

    private ButtonScript DoorButton2;
    
    // Start is called before the first frame update
    void Start()
    {
        DoorButton1 = button1.GetComponent<ButtonScript>();
        DoorButton2 = button2.GetComponent<ButtonScript>();
        

    }

    // Update is called once per frame
    void Update()
    {

        if (DoorButton1.Pulsado == true && DoorButton2.Pulsado == true)
        {
            Destroy(gameObject);
        }

    }
}
