using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FinalScreenTrigger : MonoBehaviour
{
    [SerializeField] private GameObject FinalScreen;
    [SerializeField] private AudioSource walkSFX;
    public bool finalScreen;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            finalScreen = true;
            Cursor.visible = true ;
            Time.timeScale = 0;
            walkSFX.Stop();
            FinalScreen.SetActive(true);
            
        }
        
    }
    public void ExitMainMenu()
    {
        FinalScreen.SetActive(false);
        Debug.Log("QUITTING");
        SceneLoaderSimple.Instance.LoadMainMenu();
        SceneLoaderSimple.Instance.RemoveGameScene3();

    }
}
