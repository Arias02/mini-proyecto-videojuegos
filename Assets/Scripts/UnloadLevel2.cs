using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnloadLevel2 : MonoBehaviour
{
    private bool unloaded = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && unloaded == false)
        {
            SceneLoaderSimple.Instance.RemoveGameScene2();
            unloaded = true;
        }
    }
}
